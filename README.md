# Simple Sum Rest API

This application create a Simple Api that receives a valid JSON with two numbers "A" and "B" and return their sum.
   
   1. Tools and Frameworks
   
      [Spring Boot](http://projects.spring.io/spring-boot/)
         
   
   2. Build and Run
   
      [Maven](https://maven.apache.org/)
   
      You can import as a maven application or simply running ./mvnw spring-boot:run
   
   3. Utilization
   
      curl -i -XPOST -H "Content-Type: application/json" http://localhost:8080/api/sum -d '{ "A": 100, "B": 200 }'    