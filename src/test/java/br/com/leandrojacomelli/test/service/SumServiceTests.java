package br.com.leandrojacomelli.test.service;

import br.com.leandrojacomelli.test.SimpleSumRestApiApplication;
import br.com.leandrojacomelli.test.service.exception.SumServiceException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SimpleSumRestApiApplication.class)
@WebAppConfiguration
public class SumServiceTests {

    @Autowired
    private SumService service;

    @Test
    public void ShouldSumTwoNumbers() throws SumServiceException {

        Integer a = 100;
        Integer b = 100;
        Integer result = 200;
        Assert.assertEquals(result, service.sum(a, b));
    }

    @Test(expected = SumServiceException.class)
    public void ShouldFailOnFirstStatementNull() throws SumServiceException {
        Integer a = null;
        Integer b = 100;
        service.sum(a, b);
    }

    @Test(expected = SumServiceException.class)
    public void ShouldFailOnSecondStatementNull() throws SumServiceException {
        Integer a = 100;
        Integer b = null;
        service.sum(a, b);
    }

    @Test(expected = ArithmeticException.class)
    public void ShouldFailOnPositiveOverflow() throws SumServiceException {
        Integer a = Integer.MAX_VALUE;
        Integer b = 100;
        service.sum(a, b);
    }

    @Test(expected = ArithmeticException.class)
    public void ShouldFailOnNegativeOverflow() throws SumServiceException {
        Integer a = Integer.MIN_VALUE;
        Integer b = -100;
        service.sum(a, b);
    }

}
