package br.com.leandrojacomelli.test.web.rest;

import br.com.leandrojacomelli.test.SimpleSumRestApiApplication;
import br.com.leandrojacomelli.test.service.SumService;
import br.com.leandrojacomelli.test.web.dto.SumDTO;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.annotation.PostConstruct;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.TEXT_PLAIN;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {SimpleSumRestApiApplication.class})
@WebAppConfiguration
@IntegrationTest
public class SumControllerTests {

    @Autowired
    private SumService service;

    private MockMvc rest;

    @PostConstruct
    public void setup() {

        MockitoAnnotations.initMocks(this);
        SumController sumController = new SumController(service);
        this.rest = MockMvcBuilders.standaloneSetup(sumController).build();
    }


    @Test
    public void shouldSumTwoNumber() throws Exception {
        SumDTO dto = new SumDTO();
        dto.setA(100);
        dto.setB(100);
        ObjectMapper mapper = new ObjectMapper();

        rest.perform(post("/api/sum")
                .contentType(APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsBytes(dto)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("response").value(200));
    }

    @Test
    public void shouldFailOnWrongMethod() throws Exception {
        SumDTO dto = new SumDTO();
        dto.setA(100);
        dto.setB(100);
        ObjectMapper mapper = new ObjectMapper();

        rest.perform(get("/api/sum")
                .contentType(APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsBytes(dto)))
                .andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void shouldFailOnWrongContentType() throws Exception {
        SumDTO dto = new SumDTO();
        dto.setB(100);
        dto.setB(100);
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        rest.perform(post("/api/sum")
                .contentType(TEXT_PLAIN)
                .content(mapper.writeValueAsBytes(dto)))
                .andExpect(status().isUnsupportedMediaType());

    }

    @Test
    public void shouldFailOnInvalidParameterAsBadRequest() throws Exception {
        SumDTO dto = new SumDTO();
        dto.setB(100);
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        rest.perform(post("/api/sum")
                .contentType(APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsBytes(dto)))
                .andExpect(status().isBadRequest());

    }

    @Test
    public void shouldFailOnOverFlowAsBadRequest() throws Exception {
        SumDTO dto = new SumDTO();
        dto.setA(Integer.MAX_VALUE);
        dto.setB(100);
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        rest.perform(post("/api/sum")
                .contentType(APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsBytes(dto)))
                .andExpect(status().isBadRequest());
    }

}
