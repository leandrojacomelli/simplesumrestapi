package br.com.leandrojacomelli.test.service;

import br.com.leandrojacomelli.test.service.exception.SumServiceException;
import org.springframework.stereotype.Service;

@Service
public class SumService {

    public Integer sum(Integer a, Integer b) throws SumServiceException {
        if (a == null || b == null)
            throw new SumServiceException();
        return Math.addExact(a, b);
    }
}
