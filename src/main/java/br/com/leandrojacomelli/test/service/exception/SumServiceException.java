package br.com.leandrojacomelli.test.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.BAD_REQUEST, reason="Invalid Parameters")
public class SumServiceException extends Exception {
}
