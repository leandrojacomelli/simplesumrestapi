package br.com.leandrojacomelli.test.web.dto;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class SumDTO implements Serializable{

    @JsonProperty("A")
    private Integer A;

    @JsonProperty("B")
    private Integer B;

    public Integer getA() {
        return A;
    }

    public void setA(Integer a) {
        A = a;
    }

    public Integer getB() {
        return B;
    }

    public void setB(Integer b) {
        B = b;
    }
}
