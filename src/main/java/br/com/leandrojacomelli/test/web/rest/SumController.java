package br.com.leandrojacomelli.test.web.rest;

import br.com.leandrojacomelli.test.service.SumService;
import br.com.leandrojacomelli.test.web.dto.SumDTO;
import br.com.leandrojacomelli.test.service.exception.SumServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.AbstractMap;

@RestController
public class SumController extends BaseController{


    private final SumService service;

    @Autowired
    public SumController(final SumService service) {
        this.service = service;
    }


    @RequestMapping(value = "/sum",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity sum(@RequestBody final SumDTO dto) throws SumServiceException {
        Integer response = service.sum(dto.getA(), dto.getB());
        return ResponseEntity.ok(new AbstractMap.SimpleEntry<>("response", response));

    }


}
