package br.com.leandrojacomelli.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan
public class SimpleSumRestApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimpleSumRestApiApplication.class, args);
    }


}
